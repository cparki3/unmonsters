﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class envelopeManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator envelopeAnimator;
    public GameObject thankYouMessage;
    public CardStepManager csm;
    public Animator backgroundAnimator;
    void Start()
    {
        
    }

    private void OnEnable()
    {
        showScreenshot();
    }

    public void showScreenshot()
    {
        envelopeAnimator.SetTrigger("showPhoto");
    }

    public void photoShowComplete()
    {
        Invoke("showEnvelope", 1f);
    }

    public void showEnvelope()
    {
        envelopeAnimator.SetTrigger("showEnvelope");
    }

    public void showThankyou()
    {
        thankYouMessage.SetActive(true);
        Invoke("showVideo", 3f);
        Invoke("resetApp", 5f);
    }

    public void showVideo()
    {
        csm.showVideoScreen();
    }

    public void resetApp()
    {
        csm.showSplashScreen();
        thankYouMessage.SetActive(false);
        envelopeAnimator.SetTrigger("reset");
        backgroundAnimator.SetTrigger("backToStart");
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
