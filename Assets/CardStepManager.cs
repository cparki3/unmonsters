﻿using FreeDraw;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardStepManager : MonoBehaviour
{

    public GameObject splashScreen;
    public Animator splashScreenAnimator;
    public Animator splashVideoAnimator;
    private int backgroundIndex = 0;
    public int timerAmount = 180;
    public int timeLeft = 100;

    [Header("Side Panel Controls")]
    public Canvas redPanel;
    public Canvas greenPanel;
    public Animator greenPanelAnimator;
    public bool greenPanelShowing = false;
    public Canvas bluePanel;
    public Animator bluePanelAnimator;
    public bool bluePanelShowing = false;
    public Canvas purplePanel;
    public Animator purplePanelAnimator;
    public bool purplePanelShowing = false;


    [Header("Screen Recorder Script")]
    public ScreenRecorder screenRecordScript;

    [Header("Image Sliders")]
    public GameObject scaleSlider;
    public GameObject rotationSlider;

    [Header("Signature Stuff")]
    public GameObject signatureImage;
    public GameObject signatureAnimation;
    public GameObject clearSignatureButton;
    public Drawable signatureDrawable;

    [Header("Background Select Options")]
    public SpriteRenderer backgroundSprite;
    public Sprite[] backgrounds;
    public Button prevBackgroundButton;
    public Button nextBackgroundButton;

    [Header("Particle Touch Controls")]
    public GameObject touchParticleObject;
    public ParticleSystem touchParticleSystem;
    public AudioSource touchSound;

    [Header("Sticker Managing Controls")]
    public GameObject stickerPrefab;
    public GameObject stickerParentObject;
    public StickerManager selectedSticker;
    public Vector2 stickerSpawnLocation;
    public Slider scaleSliderSlide;
    public Slider rotationSliderSlide;
    public int stickerSortOrder = 10;
    public int currentStickerCount = 0;
    public int maxStickerCount = 30;

    [Header("Message Managing Controls")]
    public GameObject messagePrefab;
    public GameObject messageParentObject;
    public MessageManager selectedMessage;
    public Vector2 messageSpawnLocation;
    public int messageSortOrder = 1000;
    public int currentMessageCount = 0;
    public int maxMessageCount = 8;

    public int currentStep = 1;

    public GameObject splashVideoScreen;

    [Header("Camera Snap Controls")]
    public Animator camSnapAnimator;
    public GameObject envelope;
    public AudioSource cameraSnapSound;
    public GameObject bayCareLogo;
    public bool sending = false;

    // Start is called before the first frame update
    void Start()
    {
#if !UNITY_EDITOR
                Cursor.visible = false;
#endif
        screenRecordScript.csm = this;
        showSplashScreen();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            showParticle();
            resetTimeout();
        }
    }

    public void resetTimeout()
    {
        timeLeft = timerAmount;
    }

    public void hideSliders()
    {
        scaleSlider.SetActive(false);
        rotationSlider.SetActive(false);
    }

    public void deselectStickers()
    {
        foreach(Transform child in stickerParentObject.transform)
        {
            child.gameObject.SendMessage("deselectSticker", null, SendMessageOptions.DontRequireReceiver);
        }
        selectedSticker = null;
    }

    public void destroyAllStickers()
    {
        foreach (Transform child in stickerParentObject.transform)
        {
            Destroy(child.gameObject);
        }
        currentStickerCount = 0;
        stickerSortOrder = 10;
    }

    public void stickerSelected()
    {
        scaleSlider.SetActive(true);
        rotationSlider.SetActive(true);
        scaleSliderSlide.value = selectedSticker.stickerGameObject.transform.localScale.x;
        float angle = selectedSticker.stickerGameObject.transform.eulerAngles.z;
        angle = (angle > 180) ? angle - 360 : angle;
        rotationSliderSlide.value = angle;
    }

    public void stickerScaleChanged()
    {
        if(selectedSticker != null)
        {
            float scaleValue = scaleSliderSlide.value;
            selectedSticker.stickerGameObject.transform.localScale = new Vector2(scaleValue, scaleValue); 
        }
        if(selectedMessage != null)
        {
            float scaleValue = scaleSliderSlide.value;
            selectedMessage.stickerGameObject.transform.localScale = new Vector2(scaleValue, scaleValue);
        }
    }

    public void stickerRotationChanged()
    {
        if (selectedSticker != null)
        {
            float rotationValue = rotationSliderSlide.value;
            selectedSticker.stickerGameObject.transform.eulerAngles = new Vector3(0, 0, rotationValue);
        }
        if(selectedMessage != null)
        {
            float rotationValue = rotationSliderSlide.value;
            selectedMessage.stickerGameObject.transform.eulerAngles = new Vector3(0, 0, rotationValue);
        }
    }

    public void createSticker()
    {
        if(currentStickerCount < maxStickerCount)
        {
            deselectStickers();
            GameObject newSticker = Instantiate(stickerPrefab, stickerSpawnLocation, Quaternion.identity, stickerParentObject.transform);
            StickerManager newStickerManager = newSticker.GetComponent<StickerManager>();
            newStickerManager.csm = this;
            newStickerManager.getRandomSticker();
            selectedSticker = newStickerManager;
            scaleSliderSlide.value = selectedSticker.stickerGameObject.transform.localScale.x;
            float angle = selectedSticker.stickerGameObject.transform.eulerAngles.z;
            angle = (angle > 180) ? angle - 360 : angle;
            newStickerManager.stickerSelected();
            rotationSliderSlide.value = angle;
            currentStickerCount++;
        }
    }

    public void deleteSticker()
    {
        if (currentStickerCount > 0 && selectedSticker != null)
        {
            Destroy(selectedSticker.gameObject);
            selectedSticker = null;
            currentStickerCount--;
            hideSliders();
        }
    }

    public void createMessage()
    {
        if (currentMessageCount < maxMessageCount)
        {
            deselectMessages();
            GameObject newMessage = Instantiate(messagePrefab, messageSpawnLocation, Quaternion.identity, messageParentObject.transform);
            MessageManager newMessageManager = newMessage.GetComponent<MessageManager>();
            newMessageManager.csm = this;
            newMessageManager.getRandomSticker();
            selectedMessage = newMessageManager;
            scaleSliderSlide.value = selectedMessage.stickerGameObject.transform.localScale.x;
            float angle = selectedMessage.stickerGameObject.transform.eulerAngles.z;
            angle = (angle > 180) ? angle - 360 : angle;
            newMessageManager.stickerSelected();
            rotationSliderSlide.value = angle;
            currentMessageCount++;
        }
    }

    public void deleteMessage()
    {
        if(currentMessageCount > 0 && selectedMessage != null)
        {
            Destroy(selectedMessage.gameObject);
            selectedMessage = null;
            currentMessageCount--;
            hideSliders();
        }
    }

    public void deselectMessages()
    {
        foreach (Transform child in messageParentObject.transform)
        {
            child.gameObject.SendMessage("deselectSticker", null, SendMessageOptions.DontRequireReceiver);
        }
        selectedMessage = null;
    }

    public void destroyAllMessages()
    {
        foreach (Transform child in messageParentObject.transform)
        {
            Destroy(child.gameObject);
        }
        currentMessageCount = 0;
        messageSortOrder = 1000;
    }

    public void messageSelected()
    {
        scaleSlider.SetActive(true);
        rotationSlider.SetActive(true);
        scaleSliderSlide.value = selectedMessage.stickerGameObject.transform.localScale.x;
        float angle = selectedMessage.stickerGameObject.transform.eulerAngles.z;
        angle = (angle > 180) ? angle - 360 : angle;
        rotationSliderSlide.value = angle;
    }

    public void showParticle()
    {
        Vector2 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        touchParticleObject.transform.position = newPosition;
        //Debug.Log(Input.mousePosition);
        if (!touchParticleSystem.isPlaying)
        {
            if (touchSound)
            {
                touchSound.PlayOneShot(touchSound.clip);
            }
           // Debug.Log("should be playing particle");
            touchParticleSystem.Play();
        }
    }

    public void nextBackground()
    {
        if(backgroundIndex == backgrounds.Length - 1)
        {
            backgroundIndex = 0;
        }
        else
        {
            backgroundIndex++;
        }
        backgroundSprite.sprite = backgrounds[backgroundIndex];
    }

    public void prevBackground()
    {
        if (backgroundIndex == 0)
        {
            backgroundIndex = backgrounds.Length -1;
        }
        else
        {
            backgroundIndex--;
        }
        backgroundSprite.sprite = backgrounds[backgroundIndex];
    }


    public void hideSplashScreen()
    {
       // Debug.Log("hide the splash screen now");
        resetAnimators();
        signatureDrawable.ResetCanvas();
        showStep(1);
        backgroundIndex = 0;
        backgroundSprite.sprite = backgrounds[backgroundIndex];
        destroyAllStickers();
        destroyAllMessages();
        //splashScreen.SetActive(false);
        //Invoke("animateSplashScreen", 2f);
        animateSplashScreen();
        //splashVideoScreen.SetActive(false);
        //splashVideoAnimator.SetBool("showVideo", false);
        timeLeft = timerAmount;
        InvokeRepeating("decreaseTimeRemaining", 0.0f, 1.0f);
    }

    public void animateSplashScreen()
    {
        splashScreenAnimator.SetBool("showSplashScreen", true);
    }

    public void decreaseTimeRemaining()
    {
        timeLeft--;
        if(timeLeft <= 0)
        {
            showSplashScreen();
            CancelInvoke("decreaseTimeRemaining");
        }
    }

    public void resetAnimators()
    {
        if (greenPanelShowing)
        {
            greenPanelAnimator.SetTrigger("HidePanel");
            greenPanelShowing = false;
        }
        if(bluePanelShowing)
        {
            bluePanelAnimator.SetTrigger("HidePanel");
            bluePanelShowing = false;
        }
        if(purplePanelShowing)
        {
            purplePanelAnimator.SetTrigger("HidePanel");
            purplePanelShowing = false;
        }
    }

    public void showSplashScreen()
    {
        sending = false;
        //splashScreen.SetActive(true);
        splashScreenAnimator.SetBool("showSplashScreen", false);
        //splashVideoScreen.SetActive(true);
        //splashVideoAnimator.SetBool("showVideo", true);
        hideBackgroundButtons();
    }

    public void showVideoScreen()
    {
        //splashVideoAnimator.SetBool("showVideo", true);
    }

    public void hideBackgroundButtons()
    {
        nextBackgroundButton.gameObject.SetActive(false);
        prevBackgroundButton.gameObject.SetActive(false);
    }

    public void showStep(int stepNum)
    {
        currentStep = stepNum;
        deselectMessages();
        deselectStickers();
        switch (stepNum)
        {
            case 1:
                redPanel.sortingOrder = 50;
                greenPanel.sortingOrder = 40;
                bluePanel.sortingOrder = 30;
                purplePanel.sortingOrder = 20;
                scaleSlider.SetActive(false);
                rotationSlider.SetActive(false);
                signatureImage.SetActive(false);
                clearSignatureButton.SetActive(false);
                signatureAnimation.SetActive(false);
                nextBackgroundButton.gameObject.SetActive(true);
                prevBackgroundButton.gameObject.SetActive(true);
                deselectStickers();
                break;
            case 2:
                redPanel.sortingOrder = 40;
                greenPanel.sortingOrder = 50;
                bluePanel.sortingOrder = 30;
                purplePanel.sortingOrder = 20;
                signatureImage.SetActive(false);
                clearSignatureButton.SetActive(false);
                signatureAnimation.SetActive(false);
                if (!greenPanelShowing)
                {
                    greenPanelAnimator.SetTrigger("ShowPanel");
                    greenPanelShowing = true;
                }
                hideBackgroundButtons();
                break;
            case 3:
                redPanel.sortingOrder = 30;
                greenPanel.sortingOrder = 40;
                bluePanel.sortingOrder = 50;
                purplePanel.sortingOrder = 20;
                scaleSlider.SetActive(false);
                rotationSlider.SetActive(false);
                signatureImage.SetActive(false);
                clearSignatureButton.SetActive(false);
                signatureAnimation.SetActive(false);
                if (!bluePanelShowing)
                {
                    bluePanelAnimator.SetTrigger("ShowPanel");
                    bluePanelShowing = true;
                }
                hideBackgroundButtons();
                deselectStickers();
                break;
            case 4:
                redPanel.sortingOrder = 20;
                greenPanel.sortingOrder = 30;
                bluePanel.sortingOrder = 40;
                purplePanel.sortingOrder = 50;
                scaleSlider.SetActive(false);
                rotationSlider.SetActive(false);
                signatureImage.SetActive(true);
                clearSignatureButton.SetActive(true);
                if (signatureDrawable.isDirty)
                {
                    signatureAnimation.SetActive(false);
                }
                else
                {
                    signatureAnimation.SetActive(true);
                }
                if (!purplePanelShowing)
                {
                    purplePanelAnimator.SetTrigger("ShowPanel");
                    purplePanelShowing = true;
                }
                hideBackgroundButtons();
                deselectStickers();
                break;
            default:
                break;
        }
    }

    public void showSignatureAnimation()
    {
        signatureAnimation.SetActive(true);
    }

    public void sendCard()
    {
        if (!sending)
        {
            sending = true;
            bayCareLogo.SetActive(true);
            screenRecordScript.CaptureScreenshot();
        }
       
    }

    public void screenshotComplete()
    {
        camSnapAnimator.SetTrigger("takePhoto");
        cameraSnapSound.PlayOneShot(cameraSnapSound.clip);
        bayCareLogo.SetActive(false);
    }
}
