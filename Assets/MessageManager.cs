﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour
{
    public Sprite[] stickerImages;
    public Image stickerUIImage;
    public GameObject stickerGameObject;
    public Button nextStickerButton;
    public Button prevStickerButton;
    public int stickerIndex = 0;
    public Outline stickerOutline;
    public Canvas stickerCanvas;

    public CardStepManager csm;
    public UIElementDragger uied;
    
    // Start is called before the first frame update

    private void Awake()
    {
        uied.mm = this;
    }

    void Start()
    {
        
    }

    public void stickerSelected()
    {
        if(csm != null)
        {
            if(csm.currentStep == 3)
            {
                csm.deselectMessages();
                csm.selectedMessage = this;
                nextStickerButton.gameObject.SetActive(true);
                prevStickerButton.gameObject.SetActive(true);
                csm.messageSelected();
                //stickerOutline.enabled = true;
                stickerCanvas.sortingOrder = csm.messageSortOrder;
                csm.messageSortOrder += 1;
            }
        }
    }

    public void deselectSticker()
    {
        nextStickerButton.gameObject.SetActive(false);
        prevStickerButton.gameObject.SetActive(false);
        stickerOutline.enabled = false;
    }

    public void getRandomSticker()
    {
        stickerIndex = Random.Range(0, stickerImages.Length);
        stickerUIImage.sprite = stickerImages[stickerIndex];
    }

    public void nextSticker()
    {
        if(stickerIndex == stickerImages.Length - 1)
        {
            stickerIndex = 0;
        }
        else
        {
            stickerIndex++;
        }
        stickerUIImage.sprite = stickerImages[stickerIndex];
    }

    public void prevSticker()
    {
        if (stickerIndex == 0)
        {
            stickerIndex = stickerImages.Length -1;
        }
        else
        {
            stickerIndex--;
        }
        stickerUIImage.sprite = stickerImages[stickerIndex];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
