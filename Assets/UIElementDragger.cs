﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIElementDragger : EventTrigger
{

    private bool dragging;
    public StickerManager sm;
    public MessageManager mm;

    public void Update()
    {
        if (dragging)
        {
            Vector2 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.parent.transform.position = newPosition;
            var pos = Camera.main.WorldToViewportPoint(transform.parent.transform.position);
            pos.x = Mathf.Clamp(pos.x, 0.1f, 0.6f);
            pos.y = Mathf.Clamp(pos.y, 0.2f, 0.9f);
            transform.parent.transform.position = Camera.main.ViewportToWorldPoint(pos);
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        
        if(sm != null)
        {
            if (sm.csm.currentStep == 2)
            {
                dragging = true;
                sm.stickerSelected();
            }
        }
        if (mm != null)
        {
            if(mm.csm.currentStep == 3)
            {
                dragging = true;
                mm.stickerSelected();
            }
        }
        
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        dragging = false;
    }
}
