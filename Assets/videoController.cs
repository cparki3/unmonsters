﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class videoController : MonoBehaviour
{
    public Material material;

    public Color color;

    public string layerName;
    public int layerOrder;

    public MeshRenderer rend;

    void Start()
    {
        color = material.color;
        rend.sortingLayerName = layerName;
        rend.sortingOrder = 3000;
    }

    void Update()
    {
        if(color != material.color)
        {
            material.color = color;
        }
    }
}
